﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Palla")
        {
            //Chiamo la void che fa cadere la piattaforma
           Invoke("Piattaformacade",0.5f); //lascio il tempo che la palla esca
        }
    }

    void Piattaformacade()
    {
        GetComponentInParent<Rigidbody>().useGravity = true;
        GetComponentInParent<Rigidbody>().isKinematic = false;

        Destroy(transform.parent.gameObject, 2f);//distrugge la piattaforma
    }
}
