﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject palla;
    Vector3 offset; //questa è la distanza tra la palla e la cam
    public float learpRate;
    public bool gameOver;

    // Start is called before the first frame update
    void Start()
    {
        
        offset = palla.transform.position - transform.position; // calcolo la distanza tra la palla e la cam
        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameOver)
        {
        Segui();
        }
    }

    void Segui()
    {
        Vector3 pos = transform.position; //calcolo la posizione della cam
        Vector3 targhetPos = palla.transform.position - offset;
        pos = Vector3.Lerp(pos, targhetPos, learpRate * Time.deltaTime);
        transform.position = pos;
    }
}
